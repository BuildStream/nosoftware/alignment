# BuildStream policies

## Hacking on BuildStream

Every policy related with hacking or developing BuildStream is described in the technical documentation: <https://buildstream.gitlab.io/buildstream/HACKING.html>

## BuildStream on Gitlab: policies

Description of the main elements on Gitlab used at BuildStream.

### Milestones

Milestone on Gitlab matches relevant events that are related with time frames like release cycles and events. The only current global milestones across the entire BuildStream Group are release cycles.

General description of how milestones work on Gitlab: <https://docs.gitlab.com/ee/user/project/milestones/>

Global milestones on Gitlab: <https://gitlab.com/groups/BuildStream/-/milestones>

### Labels

Labels allow us to structure, filter and visualise some of the Gitlab elements in different ways. Some of those labels apply to the entire BuildStream group and some only apply to specific repositories (projects).

Check the description of labels on Gitlab: <https://docs.gitlab.com/ee/user/project/labels.html>

Check the global scale labels: <https://gitlab.com/groups/BuildStream/-/labels>

The global ones are:

* Status scale:
	* Backlog: default state on Gitlab so no label needed.
	* Todo: processed elements that should be done in the future.
	* Doing: WIP
	* Review: items that are under review once the developer or contributor has finished it. 
	* Closed: items that has been processed and canceled, finished or no longer apply. Default state on Gitlab so no label needed.
* Severity scale (these labels are prioritized):
    * No label: unprocessed item, no or low priority.
    * Important: default severity for processed items that should be executed/considered.
    * Urgent: to pay attention in the immediate future when possible.
    * Critical: topics that require attention immediately when possible.
* Impact scale: this scale is restricted to the buildstream project for now.
	* high_impact: the crash/feature/task takes place frequently or is a recurrent one and has a significant impact on users everyday or key work.
	* No label: the crash/feature/task is infrequent or hard to reproduce and the impact on the users is low or hard to determine.
* Topic labels: each repository has its own labels that allow to structure tickets per topic.

Check the buildstream project specific topic labels: <https://gitlab.com/BuildStream/buildstream/labels>

Check the nosoftware repos topic labels

* communication project labels: <https://gitlab.com/BuildStream/nosoftware/communication/labels>
* alignment project labels: <https://gitlab.com/BuildStream/nosoftware/alignment/labels>

In order to propose new labels or a change in the current ones, please send a mail to the mailing list or add a ticket in the alignment repo: <https://gitlab.com/BuildStream/nosoftware/alignment/issues>

### Issue Boards

Boards allow anybody interested in BuildStream to visualise and manage (workflow) issues in a simple way. BuildStream has boards at global level (group), subgroup level (nosoftware) and project level.

* Check the description of labels on Gitlab: <https://about.gitlab.com/features/issueboard/>
* Check the global boards: <https://gitlab.com/groups/BuildStream/-/boards/580444?milestone_title=BuildStream_v1.1>&= 
* Check the buildstream repo specific boards: <https://gitlab.com/BuildStream/buildstream/boards/580451?milestone_title=BuildStream_v1.1>&=
* There is a menu in the above pages to select every available board.

There are three main groups of boards:

* Status boards: allow anybody to visualize Work in Progress (WIP) and move tickets from one status to another one so there is no need to manually change the labels, including when a ticket is closed.
* Severity boards: allow anybody to visualize how relevant the tickets are for developers, based on a variety of criteria that ends up on a notion of priority. It also is useful to move the tickets from one severity state to another one so there is no need to manually change the labels, including when a ticket is closed.
* Other boards: there are other kind of boards for specific use cases.

### Templates

The usage of issues and merge requests templates allow community members to understand which information is required for developers and contributors to fully understand and process the bug/task/request adequately. Templates are assigned per project/repo and need to be selected when creating an issue or merge request.


Check the information about templates from Gitlab: <https://docs.gitlab.com/ee/user/project/description_templates.html>

Check the templates used at BuildStream:
* buildstream repo/project templates:
	* Default template:
	* Bug template:
	* Merge request template:
* alignment/communication repos templates:
    * task template: https://gitlab.com/BuildStream/nosoftware/alignment/blob/master/.gitlab/issue_templates/task.md
    * bug template: https://gitlab.com/BuildStream/nosoftware/alignment/blob/master/.gitlab/issue_templates/bug.md
    * Merge request template: https://gitlab.com/BuildStream/buildstream/blob/master/.gitlab/merge_request_templates/bst_merge_request.md

### Technical/engineering tasks: best practices.

These are the best practices that are applied to Gitlab at BuildStream in relation with bugs and tasks. Please read them carefully and try to apply them.

#### Open/create a technical issue

In order to open a new task the best place is the green button at this page: <https://gitlab.com/BuildStream/buildstream/issues>. Please follow these considerations:
* Use the default template.
* Fill out the information required.
	* Select the milestone/version affected or related with the task.
	* If the ticket is related with the current milestone/version and older, please leave the default option.
* Save the ticket.
* Apply further metadata if you are sure about which labels to use.
	* Leave the Assignee blank unless you know who is the person responsible for that issue.
	* Consider applying topic labels if are familiar with them.
		* If the issue is just a breakdown of another task, please consider applying the same topic labels.
* Further considerations:
	* If you want to notify somebody about the ticket, please add his/her name as first comment. Try to avoid naming people for notification reasons in the ticket description.
	* It is extremely useful that you use the Related Topics field adding:
		* #"ticket id" if the ticket you just created is related with another ticket from the same repo.
		* Relative link if the ticket you just created is related with a ticket from the BuildStream group or a different group within gitlab.com.
		* A full URL if the ticket is related with a gitlab ticket under a different domain.
		* The MR associated with the ticket. Please do not forget this.
	* If your issue remains unprocessed after a few days/weeks, please ask through the project mailing list about it. It should be the exception but it might have been overlooked or remain unnoticed.  

#### Update an engineering task / ticket

Assuming the bulk of the work on engineering tasks takes places on Merge Requests (code), it is strongly recommended that you follow these advices:
* Assign the ticket to you when you start working on it. More than one person can be assigned to a single task.
* Assign the correct milestone and labels if they are not in place already.
   * If the task is being executed, please assign the label "Doing", removing the "ToDo" label.
   * Fill out the "Related Issues" field if there are other bugs or tasks that have a dependency or are related with the one it is being executed.
* Update the ticket when a relevant event takes place. In any case, update the ticket every couple of days.
    * This is very important for others to follow up your work.

#### Close an engineering task

(coming soon)
    
### Bugs: best practices
    
#### Open/create a bug

In order to open a new bug the best place is the green button at this page: <https://gitlab.com/BuildStream/buildstream/issues>. Please follow these considerations:
* Use the Bug template. Choose it from the template menu when creating a new bug.
* Fill out the information required. This is extremely important in the case of bugs. Developers will need to reproduce it in order to process it adequately.
	* Please fill out the milestone/version affected. 
		* Refer to milestones adding the "%" symbol and selecting from the available options.
		* If the affected version is the current
* Save the ticket.
* Apply further metadata if you are sure about which labels to use.
	* If the bug shows up frequently and has a big impact in your everyday work or is present while performing a key action, please consider applying the label: high_impact. Otherwise, please do not apply it.
	* If you know the person responsible for analyzing/ processing the bug, please assign the bug to that person. Otherwise, leave it unassigned. The same policy applies to any topic labels.
	* Please remember that the Bug template already have the label "bug" assigned by default.
	* Do not assign any severity or status label unless you are going to work on it yourself. In case of doubt, apply the lowest value. 
* Further considerations:
	* Please refer to the considerations made for issues since they also apply to this case.

#### Update a bug

Assuming the bulk of the work on bugs takes places on Merge Requests (code), it is strongly recommended that you follow these advices:
* Assign the bug to you. More than one person can be assigned to a single bug.
* Assign the correct milestone and labels if they are not in place already.
   * If the bug is being investigated or resolved, please assign the label "Doing", removing the "ToDo" label.
   * Fill out the "Related Issues" field if there are other bugs or tasks that have a dependency or are related with the one it is being executed.
* Update the ticket when a relevant event takes place. In any case, update the ticket every couple of days.
    * This is very important for others to follow up your work.
	
## Merge requests: best practices.

These are the different policies that the project applies to merge requests.

#### Close a Bug

(coming soon)

### Merge requests (MR): best practices

#### Open/create a merge request

Please read carefully the following policies before opening a new merge request. The best place to create a new merge request is this page: <https://gitlab.com/BuildStream/buildstream/merge_requests>. Please follow these considerations:
* Use the default template.
* Fill out the information required.
	* As in the case of the bugs, please be verbose. It will make the review process simpler and faster.
* Save the merge request.
* Apply further metadata if you are sure about which labels to use.
	* It is extremely important that you add to the merge request description the issue id/number this merge request affects to.
	* It is important to assign the merge request to a reviewer if you know who that person is. Please use the buildstream IRC channel to find out who that person is, if you can. Otherwise, leave it unassigned.
* Further considerations:
	* We do not apply for now status labels to merge requests.
	* Severity labels will be applied in very specific cases only.
	* If you want to notify somebody about the merge request, please add his/her name as first comment, starting by the character "@". Try to avoid naming people for notification reasons in the merge request  description field.
	* If your merge request remains unprocessed for over a week, please ask through IRC (#buildstream in irc.gonme.org) about it. It should be the exception but it might have been overlooked, remain unnoticed or the default reviewer is overloaded.

#### Update a merge request
	
It is strongly recommended that you follow these advices:
* Assign the Merge Request to you when you start working on it. Please remember than more than one person can be assigned to the MR.
* Assign the correct milestone and labels if they are not in place already.
   * If the MR is in progress, please assign to it the label "Doing", removing the "ToDo" label if present.
   * every MR should be related in the description field to at least one engineering task or bug. Please check this is the case. 
* Update the MR when a relevant event takes place.

#### Close a Merge Request

(coming soon)

## Roadmap policies
	
## Suggest improvements or additions to the BuildStream policy

If the suggestion refers to coding or hacking the BuildStream code related policies, please create a MR to the technical documentation: <https://gitlab.com/BuildStream/buildstream/tree/master/doc/source>

If the suggestion refers to how to manage tickets, bugs or merge requests or any other topic not directly related with coding, please create a MR to this policy instead of editing this wiki page: <https://gitlab.com/BuildStream/nosoftware/alignment/blob/master/BuildStream_policies.md>

You can always send the proposal to the <mailto:buildstream-list@gnome.org> mailing list if any of the options above fits or you are unsure about what to do.

