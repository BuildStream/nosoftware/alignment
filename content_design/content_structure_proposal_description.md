## Content Structure Full Proposal

In the same way that a software product requires an architecture, the contents of a product do too, for similar reasons, being scalability and maintainability two of the important ones.

I would like you to evaluate the following proposal which I've called "Content Structure". As usual, I am open to suggestions when it comes to naming.

There are three diagrams[1,][2],[3] that support the current proposal. Please take a look at them before reading this document. One final document includes all of the three[4].


### Content Units

The structure is based on "Content Units", a name used to avoid the association of any of those units with a specific tool, the concept of a "page" or a "file". 

For example:

* Wikis allow to embed the content of a page in another page.
* Two content units can be published initially in a single web page and, when they expand, separated into two.


I will refer to pages or files only  in specific cases.  The "Content Units" are grouped by topics and related. The relations are simple and will be developed in future diagrams.


### Target audience

Based on the BuildStream target audience, I propose for simplicity to consider the following target audiences when it comes to content design:

* Those unaware of the existence of BuildStream (99%)
* Those aware of the existence of BuildStream (1%)
	* BuildStream users (0.1%)
		* BuildStream contributors (0.001%)

Inmost occasions when I refer to users, I will also be referring to contributors. As the project, so the content, matures, it would be ideal to segment some of the above target audiences.

Occasionally, the proposal or/and content itself might refer to the following subgroups:

* Users
	* Integrators: those who use BuildStream as part of the delivery process (integration stage). 
	* Developers: those who use BuildStream as part of their development process.
* Contributors
	* Core contributors: those who contribute on regular basis, normally to specific/key areas.
	* Occasional, sporadic, opportunistic contributors: the rest of the contributors.

This might provide a hint on further target audience segmentation we can define in the coming future.


### Tools

Initially we will work with what we have:

* Gitlab:
	* The current guide.
	* Key files in repos (specially buildstream.
* The GNOME wiki


The structure is designed to be as "tool agnostic" as possible.

We will focus initially on git based content, using gitlab capabilities for it. On the wiki, we will focus on having a good "front page" and improving a little the current content. With this approach, we expect to be ready for when a web comes, reducing the effort associated to keeping the information in sync between git and the GNOME wiki.

One risk we need to strongly consider is related with the links. There are several ways to mitigate the risk. Whatever measures we apply, they will be a matter of policy so proposals on the mailing list will be required to change them. Breaking links might not just affect internal cross-references, which might have an impact on the critical paths that will be described later in thie proposal, but also on references to our content from external sources.


### Timeline

Ideally, we will have most of the content units directly related with the BuildStream 1.2 release ready for the Release Date (RD). The rest will be done little by little, having ELCE 2018 as a key milestone.


### Content management

Since BuildStream is an Open Source project, a proven way to receive meaningful contributions in contents is to define the structure, create minimal contents according to that structure and point to missing content together with simple and clear policies on how to contribute. This will be the approach I propose to follow vs the approach in which contents are only published when finished.

As you already know by now, if we want good contents we should be as serious about them as we are about code. We have a good example on the current guide. We will create a web repo in nosoftware subgroup including a bug tracked for the contents. A proposal on how to relate this ticketing system with the existing ones and the current Documentation label will be sent to the list.

A key part of the maintainability story when it comes to contents are labels/tags and URLs. We will need to pay special attention to them and create very specific policies on how to create and update them. 


### Criteria followed to structure the content units

Assuming the Open Source nature of the project and the fact that we are developing an integration tool for a pre-defined target audience, the following criteria has been followed to structure the proposed content units:


#### Expected content change rate


Some content units will have a higher change rate than others. Separate them in different content units is essential to reduce in the future the maintenance effort. It also helps to decide which tool could be appropriate for each unit. For simplicity, I have only considered the following groups:

* (almost) Static: content that is not expected to change frequently, i.e. project principles or Manifest.
* Dynamic: content that is expected to change frequently, grow overtime or have a limited life expectancy.
* Replaceable: due to the nature of our activity, there is a specific subset of content that will be replaced by new content when a specific event occurs, mostly releases. Do no mistake replaceable with erasable. In theory, these contents will not be erased but new content will play its role.
* Erasable: temporary pages that will die for exceptional reasons, like the usage of a new tool. 


#### Front page vs project and landing pages


There are two main content units we will use as "root units":

* Project page.
* Landing page.


I refer to them as pages, instead of units, because they are single pages and also because the names are popular among those who develop FLOSS "products". 

Please be aware that these pages will not be designed as the BuildStream front/home page.

The project page and the landing page might be moved to the future web, depending on design, effort and maintenance factors.


#### Releases of outcomes drive the content structure

Assuming the most common journey to become a maintainer (not the only one) being:

Humanity -> User -> Power user -> Occasional Contributor -> Core contributor -> Maintainer

at this point of maturity of the project, releases allow us to maximize the impact of our efforts to gain users while, at the same time, it help us internally to synchronize the efforts of contributors and power users with different goals and expectations. 

As the project matures and the number of users grow we might evolve our focus towards gaining contributors, for instance. The structure, with the support of the critical path definitions, should evolve too.

In my experience, using the concept of releases as criteria for defining the content structure in projects that:

* Need the kind of attention that leads them to find users with technical skills.
* Does not have a meaningful number of experienced technical product content creators.
* Cannot or do not want to heavily invest in promotion/marketing activities compared to development ones.


.... work well.


#### Other user and sister projects


BuildGrid has been considered as a "sister project". Initially, it can rely of the current structure to minimize the effort required from their side.

Freedesktop-SDK can be considered explicitly in several units, like in Pr.1 D.1, D.4 and D.5 There is plenty of room for collaboration. 

Additional user projects can also be considered in several units. I hope you agree with me that the proposed structure leaves enough room to accommodate them. Otherwise, please let me know.  

### Topic

The content has been structured in 6 different topic groups. Some units might belong to more than one. The intention is to be clear, not accurate. The current grouping has a small impact on how the final "pages" are linked in most cases. Please check the BuildStream_Content_Structure_diagram.pdf image[1].

The topics are:

* Community (Pj - project): content related with the community project, its governance and participants.
* Management  (M): content related with the management of the roadmap, development and delivery of the project outcomes. 
* Promotion: content related with the promotion of the different outcomes and activities generated by the BuildStream community.
* Code and metadata: content units directly related with the code and metadata. Please be aware that some might consider the code itself as "the content".
* BuildStream description: content  units related with the description of the outcomes together with some additional information that helps to consume them.
* Outcomes: contents directly related with the BuildStream outcomes that are "released" and/or consumed by users.
* Landing page and project page: root pages of the key/essential content with a very specific purpose.


### Content structure description


Please check the diagram[1] called BuildStream Content Structure. The following section is a description of the diagram.


#### 0. BuildStream tool landing page


This would be the starting point for those looking for a general definition of what BuildStream is and what is for. It is meant to be a static page.


#### 1. BuildStream project page 


This would be the starting/root point for those contents related with the BuildStream Open Source project.


#### C. BuildStream Code


Together with the Download page, the following gitlab files are essential:

* README files will be nothing but a summary of what is present in other pages. Links to those pages will be included in the README file and will need to be maintained.
* BuildStream will have the README file of the master branch as the entry point for contributors, instead of Master (outcome) unit initially. As the tool grows and becomes more mature, this decision might change.
* The following relations need to be consider in order to avoid some of the mentioned risks:
	* C.2 README.rst - R.2 Master, 0. Landing Page, R.3 Feature Page and R.4 Deployment Howto units.
	* C.3 LICENSE file - Pj.2 Governance+license+sponsors content units. 
	* C.4 News/Release notes file - R.3 Feature page and D. BuildStream In Detail content units.
	* C.5 Maintainers - Pj.5 Community support and Tools content unit.

Changelog has not been considered for now, but it will in the future. It is interesting information for a project focused on systems that needs to be maintained for a long time.

C.1 BuildStream download page

The Download page is the central reference not just to download the outcomes delivered by the project, but also the associated metadata. It will also have structured and direct references to all the information required to have a smooth deployment/installation experience.

This unit will include, at least, the following content:

* Versioning.
* Platforms where BuildStream is expected to be installed.
* Deployment mechanisms/technologies.
* Complementary plugins and tools required for the deployment/installation/usage of BuildStream.
* Additional content units that any user needs to consider.
* Critical path design to ensure that the journey through BuildStream contents takes place in the pre-established order. This consideration applies in fact to, at least, every unit that is part of the critical path of any considered target audience.  


C.2 README files

Short, to the point, description of what BuildStream is for and how to consume it, together with links to further information, adding context to each one of them. That additional or complementary information could be (related with):

* Documentation - D. BuildStream in detail unit
* Community support (get help) - D.5 FAQ.
* Contributing - Pj.3 How to contribute unit.
* Licensing and copyrights - C.3 LICENSE and Pj.2 Governance+license+sponsors

Relations: C.2 README.rst - R.2 Master, 0. Landing Page, R.3 Feature Page and R.4 Deployment Howto units.


C.3 COPYING/LICENSE file

This unit should include:

* Project license. Explicit description. Links.
* Exceptions (if any) that are included in the repo. Explicit description. Links.

Relations: C.3 LICENSE file - Pj.2 Governance+license+sponsors content units. A complementary proposal related with compliance will be sent.


C.4 NEWS/Release Notes file.

News related with each release (digested release notes). Further information can be considered in this content unit or new one like:

* Changelog
* Raw release notes.

Relations: C.4 News/Release notes file - R.3 Feature page and D. BuildStream In Detail content units.


C.5 Maintainers file

List of maintainers, responsibility and contact.

Relations: C.5 Maintainers - Pj.5 Community support and Tools content unit.


#### Pr. BuildStream Promotion


Content units related with promoting BuildStream among the key targets including the promotion of the different releases, talks, finished features, etc. .

Pr.1 BuildStream Out There

This content unit collects the main references and contents about BuildStream that has been published by media or community members. It will also contain the data about the impact of the tool. Overtime, this unit becomes an important resource for a variety of decisions. 

Pr.2 Social media campaign

Place to build social media campaigns, specially during the release of BuildStream. It also defines who and how the project is being promoted through social media.

Pr.3 News / Announcements

Announcements generated by the project, like release announcements. News to be consumed by community Members or supporters would be also a matter of this section/pages.

Pr.4 Blog

Project blog where contributors talk about what they have done/contributed.


#### M. BuildStream Management


Set of pages dealing with the management of the different aspects of the creation, release and maintenance of BuildStream toolset.


M.1 BuildStream roadmap

Roadmap description including the different release timelines.

M.2 BuildStream release howto

This content unit describes the management aspects of the release process. 

M.3 BuildStream development policies/guides

Policies and guides that BuildStream developers should follow in order to get their code accepted, including unit tests and documentation.

M.4 BuildStream delivery and operations policies/guides

Set of guides and policies followed by those involved in delivery and operations tasks at BuildStream project, including information related with those managing the different services affecting the development, delivery and release.


#### BuildStream in Detail


This would be the content describing in detail the tool, including the features, components, dependencies and other elements. The ideal situation would be to have a page that provides an overall context of all the features, called BuildStream in Detail, and then links to more detailed information about each feature or component. It is important to provide a clear relation between problem statement/use case and feature.

It is important to translate to the content the architecture of the tool when it comes to core vs complementary features if, like in our case, they are developed/managed slightly different, so the feature and associated content maintenance expectations are set correctly. 

It is important to link the feature descriptions to code, so the content is worth for a wide range of profiles, making the published content valuable in a wider range of situations.


D.1 Integrate with BuildStream: examples

Examples of how to use the tool. The content can be created and maintained by the project itself and/or referenced from work done outside the project. As in other cases, there should be a clear difference between what is expected to be maintained by the project participants and what is "external". This is relevant when requesting/providing support to users and when managing negative feedback on internet.


D.2 BuildStream plug-ins and other complementary features

The complementary features, like plugins, should be documented and it is this page the starting point. It is expected that some of this features will be developed outside the project so it is necessary to structure the content in a way that a user or contributor can identify what to expect from the project in terms of maintenance or/and community support.


D.3 BuildStream core features description

Detailed description of the core features shipped by BuildStream, including those who are new on each release. Ideally, this content will provide links to additional complementary documentation that would help readers to understand the use cases behind the features, the features description themselves and how to consume them.


D.4 BuildStream glossary

Each project uses specific terminology that requires explanation. In regulated or safety critical environments or in Blue Ocean[5], this need becomes almost a requirement. The same principle applies to mature markets when using terminology as a differentiation element.

BuildStream will have a glossary.


D.5 BuildStream FAQ

The FAQ is a relevant content element, often just appreciated by projects that has reached a mature state. BuildStream FAQ is proposed to become a key step in the critical path designed for two different target audiences at two different points in the "content journey", which means that this FAQ should be structured accordingly. 


#### BuildStream outcomes (portfolio)


Contents related with the different "products" offered by the BuildStream project. Each outcome should have a separate content page. So far, we have three different outcomes:

* Even releases: targeting users
* Odd releases: targeting testers (users and contributors)
* Master: targeting contributors


R.1 BuildStream releases: portfolio charter

This page will have links to the BuildStream releases, highlighting the current one, the ones "in maintenance" and those recommended for the different target groups, together with the links to the complementary information that might be related to releases, like roadmap, announcements, etc.


R.2 Master

Master as such is not released but in BuildStream portfolio, it is the default outcome for contributors. As such, this page would be the default to route first time contributors as well as occasional contributors that requires information about changes in policies and other topics that might affect Master. 


R.3 BuildStream releases feature page

The feature page is the central page for each release, that contains the information about what is released, compared with previous releases. It also includes all the necessary links to have a wide view of how to evaluate, consume and get community support for a specific release.


R.4 BuildStream deployment howto

Each release will have instructions on how to install/deploy BuildStream for the various platforms available. Part of this information would be created and maintained by the project itself and part will be available in other sites, external to the project. This page will ensure the information is available to those BuildStream users and contributors who intend to consume BuildStream.


R.5 Known issues

On every release, there are problems that has been identified by those working on the project and not yet resolved. In some cases, there are workarounds available. This page will include such information, specially valuable prior or immediately after a release. This content should provide context/summary of what is already available on the ticketing system. 


### Pj. BuildStream Project


Contents related with the BuildStream Open Source project.

Pj.1 Project Manifest (principles) , Mission and target audience definition

Page describing the principles that drive the BuildStream community, its mission and the target audience in both, users and contributors.


Pj.2 Governance + license + sponsors

This page includes the governance aspects of the project, including the licenses and the project sponsors, as well as the relation with GNOME.


Pj.3 How to participate

This page includes the information required to participate in the project including the basic communication channels and links to read as initial steps.


Pj.4 Meetings and events

This page includes the events in which the BuildStream project participates and organizes. It should include also a list of the regular meetings, how to participate and where to find the logs.


Pj.5 Community support and tools

Description of the tools used by the BuildStream community, how to get access and description of the communication channels listed. This page complements Pj.3 since this page targets existing contributors instead of new ones.

### Critical paths

One day BuildStream will dominate the world. We will be able  to look at behavioral flows on our site and see how people consume our contents and download/update our tool set just as the best retail company does. That would mean that we not just understand what our users and contributors want from us, but that we can anticipate what they might want from us tomorrow.

To get there, we need to start today, simple but start.

Having already defined our target audience to some extent was the first step. The content structure was the second one. The third one is to define our content considering a critical path, which is the journey we want to take our users through in order to consume our outcomes having a satisfactory experience. In other words, maximizing our conversion rate.

In order to start simple, we will segment our audience in four groups, defining a critical path for each one of them. The target audiences are:
* People unaware of BuildStream
* People aware of BuildStream
* BuildStream user
* BuildStream contributor

The two diagrams explain those critical paths:
* The diagram "BuildStream_Content_Structure_critical_path_per_target.pdf"[2] defines the critical path per target audience group.
* The diagram "BuildStream_Content_Structure_critical_path.pdf"[3] defines the critical path as a workflow.

Each content unit  is related with the one described in the content structure. Once we consolidate these critical paths, we will evaluate them and improve them. We will also add new ones once we have revisited our current target audience structure, lowering the granularity of our segmentation.

### Evaluation of the conversion rate: metrics

We will need to answer to following questions: are the BuildStream content helping to move people away from the Unaware group into the contributors segment group?

Which means that we need a way to evaluate if the content structure, the content themselves and the critical paths work as expected. There are tools that provide metrics and dashboards to analyze the potential answers to these questions. Before we get to this point, we can evaluate the following:
* Number of downloads of BuildStream.
* Number of regular updates of BuildStream.
* Hits per page.
* Permanence time per page.
* Source of the hit (search engine, social media, another BuildStream page...)

which will provide us enough information to start learning about how well are we doing in terms of designing the contents the different target audiences need. Obviously the above information will only provide us trends in a blur picture, but overtime that is a good base to improve.


## Further resources

The following resources are recommended: 

[1] BuildStream_Content_Structure_diagram.pdf: https://gitlab.com/BuildStream/nosoftware/alignment/blob/master/content_design/BuildStream_Content_Structure_diagram.pdf

[2] BuildStream_Content_Structure_critical_path_per_target.pdf: https://gitlab.com/BuildStream/nosoftware/alignment/blob/master/content_design/BuildStream_Content_Structure_critical_path_per_target.pdf

[3] BuildStream_Content_Structure_critical_path.pdf: https://gitlab.com/BuildStream/nosoftware/alignment/blob/master/content_design/BuildStream_Content_Structure_critical_path.pdf

[4] BuildStream_Content_Structure_all.pdf: https://gitlab.com/BuildStream/nosoftware/alignment/blob/master/content_design/BuildStream_Content_Structure_all.pdf

[5] https://en.wikipedia.org/wiki/Blue_Ocean_Strategy

