﻿# Proposal to apply Gitlab features to BuildStream

This proposal has the following objectives:

* Get better prepared for an increase of contributors/contributions.
* Improve visibility of "work in progress" (WIP) as initial step towards acquiring progress forecast capabilities. 
* Take a step towards having release based roadmaps as a way to have transparent conversations about what to expect in coming releases. 
* Keep entropy under control while keeping processes simple and lightweight so limited maintenance/management effort is required.

  
Rationale

This proposal has the following objectives:

* Prepare the project to sustain the current increase rate of contributors.
* Improve "work in progress" (WIP) visibility as initial step towards acquiring progress forecast capabilities. 
* Take a further step towards release enhanced and participative roadmaps. 
* Keep entropy under control while keeping processes simple and lightweight so limited maintenance/management effort is required.


 ## Rationale

The number of developers involved in BuildStream is increasing so the amount work in progress. The number and complexity of dependencies among people and tasks is increasing too, as well as the tool itself. The idea behind this proposal is to tackle some of the risks associated with current growth. This proposal take advantage of some of the simplest Gitlab capabilities to improve the visibility of the different activities that today and in the future will be managed through Gitlab.

Improving visibility, specially of the WIP, is a key step towards:

* Being able to forecast based on facts/data instead of simply estimate.
* Better management of expectations as individuals and as a project.
* Increase transparency, which provides many benefits, specially working in the open.
* Reduce the threshold for potential new contributors to jump on high impact tasks. 
* Many others...


## Description

The proposal is to make use of the following items on Gitlab:

* Milestones
* Labels
* Boards
* Templates

in a coherent and structured way. 

### Milestones

Milestones allow us to structure all activities in groups and relate these group of actions with time frames. This is extremely useful for roadmapping and visualization, specially when releases are approaching.

General description of how milestones work on Gitlab: https://docs.gitlab.com/ee/user/project/milestones/

Proposal: make use of milestones as a high level structure of our activities:

* Create a global milestone for the ongoing work: buildstream_v1.1
* Create another global milestone for the future work: buildstream_v1.2

By creating these two milestones and make them global, that is, available across the entire BuildStream Group, we will be able to structure all necessary items across every project, including those in the nosoftware Subgroup.

Check the examples I have created for the nosoftware Subgroup: https://gitlab.com/groups/BuildStream/nosoftware/-/milestones

* You can see two milestones corresponding to releases available across the Subgroup together with two additional milestones, corresponding to events, affecting only the repo/project "communication"

### Labels

Labels allow us to structure, filter and visualise some of the Gitlab elements in different ways. The create an impact in other elements and they introduce complexity and effort as they grow in number and usage. So the general approach is to have the minimum possible.

Check the description of labels on Gitlab: https://docs.gitlab.com/ee/user/project/labels.html

Proposal: create 4 different group of labels and deprecate some of the existing ones:

* Severity: 4 levels
	* No label: unprocessed item, no or low priority.
	* Important: default severity for processed items that should be executed/considered.
	* Urgent: to pay attention in the immediate future when possible.
	* Critical: topics that require attention immediately when possible. This label already exists.
* State: 5 states proposed although two of  them are default states on Gitlab.
	* Backlog: default state on Gitlab so no label needed.
	* Todo: processed elements that should be done in the future.
	* Doing: WIP
	* Review: items that are under review. 
	* Closed: items that has been processed and cancelled, finished or no longer apply. Default state on Gitlab so no label needed.
* Topic/nature: the proposal do not add nor changes the meaning of any label. They are currently being used.  
	* Needs Backport
	* Regression
	* Bug: this label is promoted to group level so it can be used in every repo.
	* Enhancement
	* Documentation
	* Refactoring
	* Reproducibility
	* Tests
	* Logging
	* Frontend
	* Infrastructure
* Special labels:
	* Blocker: as today, this label is meaningful for releases. It becomes the higher level of severity but has implications that the proposed severity labels do not have.
* Labels to deprecate. It is pointed how to handle these cases without labels, which reduces complexity:
	* Invalid: not being used. Could be stated as a comment and close the issue.
	* Fixed: not being used. Could be stated as a comment and close the issue.
	* Wontfix: not being used. Could be stated as a comment and close the issue.
	* Minor: not being used. Severity. Already considered with no label.
	* Needinfo: not being used. Reassignment back to the item creator instead of a label.
	* Needs Backport: used only once. Comment + reassignment back to creator.

Severity and State labels are promoted as Group level labels so they would be shared across every Project, including those in nosoftware Subgroup (for non-technical tasks). Severity labels are structured as priority labels which has an impact when using the default filters.

Check the examples of nosoftware:

* Severity and State labels shared across all projects included in nosoftware: https://gitlab.com/groups/BuildStream/nosoftware/-/labels
* Labels assigned to the communication project, where the Severity and State labels are inherited: https://gitlab.com/BuildStream/nosoftware/communication/labels
* Labels assigned to the alignment project, where the Severity and State labels are inherited: <ttps://gitlab.com/BuildStream/nosoftware/alignment/labels

### Issue Boards

Boards allow you to visualise and manage issues in a simple way. Several boards can be created for each Group, Subgroup and Project to meet different user profile needs. But the key feature is that issue boards will allow BuildStream to create simple workflows so managing issues states or severity can be done by simply moving the issues within the board.

* Check the description of labels on Gitlab: https://about.gitlab.com/features/issueboard/
* Check how to create simple workflows: https://docs.gitlab.com/ee/user/project/issue_board.html#creating-workflows

Proposal: taking advantage of the proposed milestones as well as the state and the severity labels, we will create at least the following 2 set of boards at the BuildStream Group level:

* Workflow boards:
	* Per milestone.
		* Check this example from nosoftware: https://gitlab.com/groups/BuildStream/nosoftware/-/boards/567536?milestone_title=BuildStream_v1.1&=
	* All tickets
		* Check this example from nosoftware: https://gitlab.com/groups/BuildStream/nosoftware/-/boards?= 
	* Just critical issues.
		* Check this example from nosoftware: https://gitlab.com/groups/BuildStream/nosoftware/-/boards/567534?=&label_name[]=Critical
	* Just bugs.
		* Here you can see an example from Gitlab: https://gitlab.com/gitlab-org/gitlab-ce/boards/353106?=&label_name[]=bug

* Severity boards:
	* Per milestone: only current and future milestones, so two boards.
	* All tickets
		* Check the example from nosoftware: https://gitlab.com/groups/BuildStream/nosoftware/-/boards/567539?=
	* Just bugs.

Similar boards will be created for the BuildStream Project, so only technical issues would be visualised in those.

### Templates

The usage of issues and merge requests templates will have a positive effect so creators include the right information in the right order. This not just benefits the completeness of issues and MR, but also helps reviewers and those who need to act on them. 

The use of templates is particularly beneficial for newcomers and infrequent  contributors since it sets the right expectations about what information from them is required so the ping-pong process with reviewers is minimised.

A third benefit is that templates might include default metadata which not just reduces the time spent in adding them to the ticket, but also helps to process them when no additional metadata is added by a user.

Templates need to be selected when creating an issue or MR.

Check the information about templates from Gitlab: <https://docs.gitlab.com/ee/user/project/description_templates.html>

Proposal: this only applies to issue and merge request templates applied to BuildStream Project only.

* Issues:
	* Default template: unless an issue creator selects a specific template, this is the one that will be used.
	* Bug template: template specifically for opening bugs.
* Merge requests:
	* Default merge request template

The content of each template will be discussed later on.

Check the examples from nosoftware for issue templates:

* Bug template: https://gitlab.com/BuildStream/nosoftware/alignment/blob/master/.Gitlab/issue_templates/bug.md
* Task template (as idea for default issue template): https://gitlab.com/BuildStream/nosoftware/alignment/blob/master/.Gitlab/issue_templates/task.md

## Other repos

Those proposed enhancements that are only applicable to the BuildStream Project will be rolled out to the rest of the Projects (repos) later on. In the case of the topic labels, a per project study needs to be carried on. Agustin will talk directly with the Master of each one of the Projects about it.

## Risk management

The following risks apply to the proposal:


* Unrealistic/fixed expectations of commitment:
	* By assigning severity labels to issues and MR, some might imply that there is a direct relation between the assigned values and time to completion. 
		* Mitigation: in the policy document it will be clear that there is no relation between severity and time to completion.
	* The usage of the metadata fields Due Date and Start Date in issues, MR and milestone are taken as commitments.
		* Mitigation: in the policy document it will be clear that there are no explicit or implicit commitments. The values assigned to these fields are taken as estimations or, in the best scenario, forecast, so they always have associated uncertainty. They are also used for visualization and notification purposes.

* Perception of assignment or handling of metadata as mandatory, specially in the case of community members.
	* The usage of metadata and workflows consumes time and requires a certain level of familiarity with the general usage policy. In some cases, it will be a matter of interpretation. There is a risk that peole perceive that the usage of the proposed elements (milestones, labels, boards and templates) is mandatory
		* Mitigation: the adoption of these policies will be promoted, not enforced, providing the chance to newcomers and occasional contributors to not follow them, focusing first on the contributions themselves. Templates will be our friend here.[BuildStream prioritises contributions over processes. 

* Unjustified increase of the maintenance effort.
	* Applying these enhancements require an additional and sustained effort making easier to scale up but harder to scale down the management effort, potentially affecting sustainability.
		* Mitigation: the proposal has been designed to limit the day to day maintenance effort and to ensure that if the management capacity drops, the project can handle it while keeping visibility in better shape than it is today.


## Next steps and final considerations

After reading this proposal, the next steps should be:

* Discussion of the proposal. Consensus generation.
* Creation of a simple policy document and publication on the wiki to support the changes.
* Implementation of the changes. Test.
* Communication of the changes.
* Evaluation of the changes and their impact.

I would like to point some final considerations:

* After the changes are applied, we need to assume that some small improvements might be needed in order to better adapt the proposal to reality.
* Once these changes are settle, my next goal is to define a roadmap workflow so there is a simple and easy way to visualise the process to turn requests into engineering tasks.  
* Many additional improvements can be done but we need to walk before running. The above proposal is what I consider the minimum to achieve the goals with the lowest impact on developers.
* I understand that this proposal is long and tedious but it is relevant to you so please have a look. I will answer the questions immediately. I will go over the comments and opinions in a few days, giving time for everybody to distil it.

Agustin Benito Bethencourt
agustin.benito@codethink.co.uk
